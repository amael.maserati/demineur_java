import java.util.Scanner;

public class Demineur extends Plateau{
    /** cet attribut définit si la partie est perdue */
    private boolean gameOver;
    /** cet attribut modélise le score */
    private int score;
    /** permet de créer le démineur
     * @param nbLignes le nombre de lignes du jeu
     * @param nbColonnes le nombre de colonnes du jeu
     * @param pourcentage le pourcentage de bombes du jeu
     */
    public Demineur(int nbLignes, int nbColonnes, int pourcentage){
        super(nbLignes, nbColonnes, pourcentage);
        this.gameOver = false;
        this.score = 0;
    }
    /** permet d'obtenir le score actuel
     * @return le score
     */
    public int getScore(){
        return this.score;
    }
    /** permet de révéler une case voire ses voisins selon certaines conditions
     * @param x la ligne de la case
     * @param y la colonne de la case
     */
    public void reveler(int x, int y) {
        if (this.getCase(x, y).contientUneBombe()) {
            for (int i = 0; i < this.getNbLignes(); ++i) {
                for (int j = 0; j < this.getNbColonnes(); ++j) {
                    if (!this.getCase(i,j).estDecouverte()) {
                        this.getCase(i, j).reveler();
                    }
                }
            } 
            gameOver = true;
        }
        int nbCasesDecouvertes = 0;
        for (int l = 0; l < this.getNbLignes(); ++l) {
            for (int col = 0; col < this.getNbColonnes(); ++col) {
                if (this.getCase(l, col).estDecouverte()) { nbCasesDecouvertes ++;}
            }
        }
        if (nbCasesDecouvertes == 0 || this.getCase(x, y).nombreBombesVoisines() == 0) {
            for (Case c : this.getCase(x,y).getVoisines()) {
                if (!c.contientUneBombe() && !c.estDecouverte()) {
                    c.reveler();  
                }
            }
        }
        this.getCase(x,y).reveler();
        for (int i = 0; i < this.getNbLignes(); ++i) {
            for (int j = 0; j < this.getNbColonnes(); ++j) {
                if (this.getCase(i,j).estDecouverte() && !this.getCase(i,j).contientUneBombe() && this.getCase(i,j).nombreBombesVoisines() == 0) {
                    for (CaseIntelligente c : this.getCase(i,j).getVoisines()) {
                        c.reveler();
                    }
                }
                for (int k = this.getNbLignes() - 1; k > -1; --k) {
                    for (int l = this.getNbColonnes() - 1; l > -1; --l) {
                        if (this.getCase(k,l).estDecouverte() && this.getCase(k,l).nombreBombesVoisines() == 0) {
                            for (CaseIntelligente c : this.getCase(k,l).getVoisines()) {
                                c.reveler();
                            }
                        }
                    }
                }
            }
        }
        if (gameOver == false) {
            score ++;
        }        
    }
    /** permet de marquer une case
     * @param x la ligne de la case
     * @param y la colonne de la case
     */
    public void marquer(int x, int y){
        this.getCase(x,y).marquer();
    }
    /** permet de savoir si la partie est gagnée
     * @return true si la partie est gagnée, false sinon
     */
    public boolean estGagnee(){
        int nbCasesAvecBombes = 0;
        for(int l = 0; l < this.getNbLignes(); ++l) {
            for (int col = 0; col < this.getNbColonnes(); ++col) {
                if (this.getCase(l, col).contientUneBombe()) {
                    nbCasesAvecBombes ++;
                }
            }
        }
        int nbCasesRevelees = 0;
        for(int i = 0; i < this.getNbLignes(); ++i) {
            for (int j = 0; j < this.getNbColonnes(); ++j) {
                if (this.getCase(i, j).estDecouverte()) {
                    nbCasesRevelees ++;
                }
            }
        }
        if ((this.getNbLignes() * this.getNbColonnes()) - nbCasesAvecBombes == nbCasesRevelees) {
            return true;
        }
        return false;
    } 
        
    /** permet de savoir si une partie est perdue
     * @return true si elle est perdue
     */
    public boolean estPerdue() {
        return this.gameOver;
    }
    /** permet de tout réinitialiser */
    public void reset(){
        super.reset();
        this.gameOver = false;
        this.score = 0;
    }
    /** permet d'afficher le jeu */
    public void affiche(){
        System.out.println("JEU DU DEMINEUR");
        // affichage de la bordure supérieure
        System.out.print("  ");
        for (int j=0; j<this.getNbColonnes(); j++){
            System.out.print("  "+j+" ");
        }
        System.out.print(" \n");
        
        // affichage des numéros de ligne + cases
        System.out.print("  ┌");
        for (int j=0; j<this.getNbColonnes()-1; j++){
                System.out.print("───┬");
        }
        System.out.println("───┐");
        
        // affichage des numéros de ligne + cases
        for (int i = 0; i<this.getNbLignes(); i++){
            System.out.print(i+" ");
            for (int j=0; j<this.getNbColonnes(); j++){
                System.out.print("│ "+this.getCase(i, j).toString() + " ");
            }
            System.out.print("│\n");
            
            if (i!=this.getNbLignes()-1){
                // ligne milieu
                System.out.print("  ├");
                for (int j=0; j<this.getNbColonnes()-1; j++){
                        System.out.print("───┼");
                }
                System.out.println("───┤");
            }
        }

        // affichage de la bordure inférieure
        System.out.print("  └");
        for (int j=0; j<this.getNbColonnes()-1; j++){
                System.out.print("───┴");
        }
        System.out.println("───┘");
        
        // affichage des infos 
        System.out.println("Nombres de bombes à trouver : " + this.getNbTotalBombes());
        System.out.println("Nombres de cases marquées : " + this.getNbCasesMarquees());
        System.out.println("Score : " + this.getScore());
    }

    /** permet de recommencer une partie */
    public void nouvellePartie(){
        this.reset();
        this.poseDesBombesAleatoirement();
        this.affiche();
        Scanner scan = new Scanner(System.in).useDelimiter("\n");

        while (!this.estPerdue() || this.estGagnee()){
            System.out.println("Entrer une instruction de la forme R 3 2 ou M 3 2\npour Révéler/Marquer la case à la ligne 3 et à la colonne 2");
            String [] s = scan.nextLine().split(" ");
            String action = s[0];
            int x = Integer.valueOf(s[1]);
            int y = Integer.valueOf(s[2]);
            if (action.equals("M") || action.equals("m"))
                this.marquer(x, y);
            else if (action.equals("R") || action.equals("r"))
                this.reveler(x, y);
            this.affiche();
        }
        if (this.gameOver){
            System.out.println("Oh !!! Vous avez perdu !");
        }
        else{
            System.out.println("Bravo !! Vous avez gagné !");
        }
    }
}
