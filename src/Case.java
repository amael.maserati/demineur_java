public class Case {
    /** cet attribut définit si une case est découverte */
    private boolean estDecouverte;
    /** cet attribut modélises si une case est marquée */
    private boolean estMarquee;
    /** cet attribut modélise si la case contient une bombe */
    private boolean contientUneBombe;
    /** permet de créer une case */
    public Case() {
        this.estDecouverte = false;
        this.estMarquee = false;
        this.contientUneBombe = false;
    }
    /** permet réinitialiser la partie */
    public void reset() {
        this.estDecouverte = false;
        this.estMarquee = false;
        this.contientUneBombe = false;
    }
    /** permet de poser une bombe sur une case */
    public void poseBombe() {
        this.contientUneBombe = true;
    }
    /** permet de savoir si une case est découverte 
     * @return vrai si la case est découverte
    */
    public boolean estDecouverte() {
        return this.estDecouverte;
    }
    /** permet de savoir si une case est marquée
     * @return vrai si la case est marquée
     */
    public boolean estMarquee() {
        return this.estMarquee;
    }
    /** permet de savoir si une case contient une bombe
     * @return vrai si la case contient une bombe
     */
    public boolean contientUneBombe() {
        return this.contientUneBombe;
    }
    /** permet de révéler une case
     * @return vrai pour considérer la case révélée
     */
    public boolean reveler() {
        if (this.estDecouverte()) {this.estDecouverte = true;}
        else {this.estDecouverte = true;}
        return this.estDecouverte;
    }
    /** permet de marquer une case
     * @return vrai pour considérer la case marquée
     */
    public boolean marquer() {
        if (this.estMarquee()) {
            this.estMarquee = false;
            return false;
        }
        else {
            this.estMarquee = true;
            return true;
        }
    }
}
