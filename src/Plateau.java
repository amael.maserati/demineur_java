import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Plateau {
    /** cet attribut modélise le nombre de lignes du plateau */
    private int nbLignes;
    /** cet attribut modélise le nombre de colonnes du plateau */
    private int nbColonnes;
    /** cet attribut modélise le pourcentage de bombes présentes sur le plateau */
    private int pourcentageDeBombes;
    /** cet attribut modélise le nombre de bombes présentes sur le plateau */
    private int nbBombes;
    /** cet attribut modélise une liste de listes de cases intelligentes pour obtenir des lignes et des colonnes */
    private List<List<CaseIntelligente>> lePlateau;
    /** permet de créer le plateau
     * @param nbLignes le nombre de lignes du plateau
     * @param nbColonnes le nombre de colonnes
     * @param pourcentage le pourcentage de bombes
     */
    public Plateau(int nbLignes, int nbColonnes, int pourcentage) {
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
        this.pourcentageDeBombes = pourcentage;
        this.lePlateau = new ArrayList<>();
        this.creerLesCasesVides();
        this.rendLesCasesIntelligentes();
    }
    /** permet de créer les cases du plateau vides */
    private void creerLesCasesVides() {
        for (int x = 0; x < this.getNbLignes() ; ++x) {
            List<CaseIntelligente> ligne = new ArrayList<>();
            for (int y = 0; y < this.getNbColonnes(); ++y) {
                ligne.add(new CaseIntelligente());
            }
            this.lePlateau.add(ligne);
        }
    }
    /** permet de rendre les cases intelligentes */
    private void rendLesCasesIntelligentes(){
        for (int x = 0; x < this.getNbLignes() ; ++x) {
            for (int y = 0; y < this.getNbColonnes(); ++y) {
                CaseIntelligente caseRegardee = this.getCase(x, y);
                for (int l = x-1; l <= x+1; ++l) {
                    for (int col = y-1; col <= y+1; ++col) {
                        if (l < this.getNbLignes() && l >= 0 && col < this.getNbColonnes() && col >= 0) {
                            caseRegardee.ajouteVoisine(this.getCase(l, col));
                        }
                    }
                }
            }
        }
    }
    /** permet de placer des bombes aléatoirement sur le plateau */
    protected void poseDesBombesAleatoirement(){
        Random generateur = new Random();
        for (int x = 0; x < this.getNbLignes(); x++){
            for (int y = 0; y < this.getNbColonnes(); y++){
                if (generateur.nextInt(100)+1 < this.pourcentageDeBombes){
                    this.poseBombe(x, y);
                    this.nbBombes = this.nbBombes + 1;
                }
            }
        }
    }
    /** permet d'obtenir le nombre de lignes du plateau
     * @return le nombre de lignes
     */
    public int getNbLignes() {
        return this.nbLignes;
    }
    /** permet d'obtenir le nombre de colonnes
     * @return le nombre de colonnes
     */
    public int getNbColonnes() {
        return this.nbColonnes;
    }
    /** permet d'obtenir le nombre total de bombes sur le plateau
     * @return le nombre de bombes
     */
    public int getNbTotalBombes() {
        return this.nbBombes;
    }
    /** permet d'obtenir une case intelligente selon une position
     * @param ligne la ligne sur laquelle est placée la case
     * @param colonne la colonne sur laquelle est placée la case
     * @return la case intelligente 
     */
    public CaseIntelligente getCase(int ligne, int colonne) {
        return this.lePlateau.get(ligne).get(colonne);
    }
    /** permet d'obtenir le nombre de cases marquées
     * @return le nombre de cases marquées
     */
    public int getNbCasesMarquees() {
        int nbCasesMarquees = 0;
        for (int x = 0; x < this.getNbLignes(); ++x) {
            for (int y = 0; y < this.getNbColonnes(); ++y) {
                if (this.getCase(x, y).estMarquee()) {
                    nbCasesMarquees ++;
                }
            }
        }
        return nbCasesMarquees;
    }
    /** permet de poser une bombe sur une position
     * @param x la ligne
     * @param y la colonne
     */
    public void poseBombe(int x, int y) {
        getCase(x,y).poseBombe();
    }
    /** permet de réinitialiser la partie */
    public void reset() {
        for (int x = 0; x < this.getNbLignes(); ++x) {
            for (int y = 0; y < this.getNbColonnes(); ++y) {
                this.getCase(x, y).reset();
            }
        }
        this.nbBombes = 0;
    }
}
