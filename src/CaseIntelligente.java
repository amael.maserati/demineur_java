import java.util.ArrayList;
import java.util.List;

public class CaseIntelligente extends Case{
    /** cet attribut modélise une liste de cases intelligentes voisines d'une case */
    private List<CaseIntelligente> lesVoisines;
    /** permet de créer une case intelligente et la liste de ses voisines */
    public CaseIntelligente() {
        super();
        this.lesVoisines = new ArrayList<>();
    }
    /** permet d'ajouter les voisines d'une case intelligente dans sa liste de voisines
     * @param uneCase la case intelligente pour laquelle on crée la liste de voisines
     */
    public void ajouteVoisine(CaseIntelligente uneCase) {
        this.lesVoisines.add(uneCase);
    }
    /** permet de compter le nombre de bombes voisines d'une case intelligente
     * @return le nombre de bombes voisines
     */
    public int nombreBombesVoisines() {
        int nbBombesVoisines = 0;
        for (Case c : this.lesVoisines) {
            if (c.contientUneBombe()) {
                nbBombesVoisines ++;
            }
        }
        return nbBombesVoisines;
    }
    /** permet d'obtenir les voisines d'une case 
     * @return la liste des cases intelligentes voisines
    */
    public List<CaseIntelligente> getVoisines() {
        return this.lesVoisines;
    }
    @Override
    /** permet d'afficher dans le terminal le démineur par rapport aux bombes, aux drapeaux et aux cases */
    public String toString() {
    String chaine = "";
    if (this.estMarquee()) { return "?";}
    if (!this.estMarquee() && !this.estDecouverte()) { return " ";}
    if (this.contientUneBombe() && this.estDecouverte()) { return "@";}
    return chaine + this.nombreBombesVoisines();
        }
    }
